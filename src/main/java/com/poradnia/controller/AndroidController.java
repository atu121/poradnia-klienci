package com.poradnia.controller;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.poradnia.service.UserService;

@RestController
public class AndroidController {

	@Autowired
	private DataSource dataSource;

	@Autowired
	UserService userService;

	@Autowired
	BCryptPasswordEncoder encoder;

	@RequestMapping(value = "rest/get", method = RequestMethod.POST)
	@ResponseBody
	public String execute(@RequestBody String query) throws SQLException {
		ResultSet executeQuery = dataSource.getConnection().prepareStatement(query).executeQuery();
		dataSource.getConnection().close();
		ResultSetMetaData rsmd = executeQuery.getMetaData();
		int columnCount = rsmd.getColumnCount();
		String em = null;
		while (executeQuery.next()) {
			if (em == null) {
				em = "@";
			} else {
				em = em + "@";
			}
			for (int i = 1; i <= columnCount; i++) {
				em = em + executeQuery.getString(i);
				if (i != columnCount) {
					em = em + ";";
				}
			}
		}
		return em;
	}

	@RequestMapping(value = "rest/insert", method = RequestMethod.POST)
	@ResponseBody
	public void executeInsert(@RequestBody String query) throws SQLException {
		boolean execute = dataSource.getConnection().prepareStatement(query).execute();
		dataSource.getConnection().close();
	}

}