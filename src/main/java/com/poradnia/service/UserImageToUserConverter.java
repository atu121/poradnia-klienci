package com.poradnia.service;

import org.springframework.stereotype.Component;

import com.poradnia.model.User;
import com.poradnia.model.UserImage;

@Component
public class UserImageToUserConverter {

	
	public User convert(UserImage userImage){
		User user = new User(); 
		user.setCity(userImage.getUser().getCity());
		user.setComment(userImage.getUser().getComment());
		return null;
		
	}
}
